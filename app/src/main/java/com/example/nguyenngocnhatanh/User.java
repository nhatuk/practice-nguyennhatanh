package com.example.nguyenngocnhatanh;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "desc")
    public String desc;

    @ColumnInfo(name = "email")
    public String email;
}
