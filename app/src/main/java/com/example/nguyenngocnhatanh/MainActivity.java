package com.example.nguyenngocnhatanh;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText etname, desc;
    private CheckBox checkBox1;
    private Button btButton;

    AppDatabase appDataBase;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDataBase = AppDatabase.getAppDatabase(this);
        etname = findViewById(R.id.etName);
        desc = findViewById(R.id.desc);
        checkBox1 = findViewById(R.id.checkBox1);
        insertUser();
        getAllUser();
    }

    private void submitForm(){

    }



    private void insertUser(){
        User user = new User();
        user.desc = "this is desc";
        user.email = "this is email";
        appDataBase.userDao().insertUser(user);
    }

    private void updateUser(){
        User user = new User();
        user.email = "update desc";
        appDataBase.userDao().updateUser(user);
    }

    private void deleteUser(int id){
        User user = appDataBase.userDao().findUserById(id);
        appDataBase.userDao().deleteUser(user);
    }

    private void getAllUser(){
        List<User> user = appDataBase.userDao().getAllUser();
        for (User user1: user){
            Log.d("TAG", "id :" +user1.id+ "desc :"+user1.desc+ "email :" +user1.email);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



}